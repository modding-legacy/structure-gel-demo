package com.legacy.dungeons_plus.data.providers;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;
import com.legacy.dungeons_plus.DungeonsPlus;
import com.legacy.dungeons_plus.data.DPTags;
import com.legacy.dungeons_plus.registry.DPBlocks;
import com.legacy.dungeons_plus.registry.DPItems;
import com.legacy.dungeons_plus.registry.DPLoot;
import com.legacy.dungeons_plus.registry.DPStructures;
import com.legacy.structure_gel.api.registry.registrar.StructureRegistrar;

import net.minecraft.Util;
import net.minecraft.advancements.critereon.EnchantmentPredicate;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.ItemPredicate;
import net.minecraft.advancements.critereon.MinMaxBounds;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.data.loot.EntityLootSubProvider;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.data.loot.LootTableSubProvider;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.alchemy.Potions;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.saveddata.maps.MapDecoration;
import net.minecraft.world.level.storage.loot.BuiltInLootTables;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.LootTable.Builder;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.entries.LootPoolEntryContainer;
import net.minecraft.world.level.storage.loot.entries.LootTableReference;
import net.minecraft.world.level.storage.loot.entries.TagEntry;
import net.minecraft.world.level.storage.loot.functions.EnchantRandomlyFunction;
import net.minecraft.world.level.storage.loot.functions.EnchantWithLevelsFunction;
import net.minecraft.world.level.storage.loot.functions.ExplorationMapFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemConditionalFunction;
import net.minecraft.world.level.storage.loot.functions.LootingEnchantFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemDamageFunction;
import net.minecraft.world.level.storage.loot.functions.SetNameFunction;
import net.minecraft.world.level.storage.loot.functions.SetNbtFunction;
import net.minecraft.world.level.storage.loot.functions.SetPotionFunction;
import net.minecraft.world.level.storage.loot.functions.SmeltItemFunction;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemEntityPropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemKilledByPlayerCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemRandomChanceCondition;
import net.minecraft.world.level.storage.loot.predicates.MatchTool;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;

public class DPLootProv extends LootTableProvider
{
	public DPLootProv(PackOutput packOutput)
	{
		//@formatter:on
		super(packOutput, Set.of(), List.of(new LootTableProvider.SubProviderEntry(DPBlockLoot::new, LootContextParamSets.BLOCK), new LootTableProvider.SubProviderEntry(DPEntityLoot::new, LootContextParamSets.ENTITY), new LootTableProvider.SubProviderEntry(DPChestLoot::new, LootContextParamSets.CHEST), new LootTableProvider.SubProviderEntry(DPArchaeologyLoot::new, LootContextParamSets.ARCHAEOLOGY)));
		//@formatter:off
	}

	@Override
	protected void validate(Map<ResourceLocation, LootTable> map, ValidationContext context)
	{
		// Not validating anything because entity loot tables will fail since the vanilla one technically doesn't exist
		//map.forEach((location, table) -> LootTables.validate(context, location, table));
	}

	private static class DPChestLoot implements LootTableSubProvider, LootPoolUtil
	{
		@Override
		public void generate(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
		{
			this.tower(consumer);
			this.reanimatedRuins(consumer);
			this.leviathan(consumer);
			this.snowyTemple(consumer);
			this.warpedGarden(consumer);

			this.soulPrison(consumer);
			this.endRuins(consumer);
		}

		private void tower(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
		{
			//@formatter:off
			consumer.accept(DPLoot.Tower.CHEST_COMMON, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.BONE, 1, 3).setWeight(1),
							item(Items.GUNPOWDER, 1, 2).setWeight(1),
							item(Items.ROTTEN_FLESH, 2, 4).setWeight(1),
							item(Items.STRING, 1, 3).setWeight(1),
							item(Items.FEATHER, 1, 2).setWeight(1),
							item(Items.FLINT, 1, 2).setWeight(1),
							tagEntry(DPTags.Items.LOOT_TOWER_COMMON, 1, 3)
						)).setRolls(UniformGenerator.between(3, 6)),
					pool(ImmutableList.of(
							item(Items.IRON_NUGGET, 1, 3).setWeight(3),
							item(Items.REDSTONE, 1, 3).setWeight(2),
							item(Items.LAPIS_LAZULI, 2, 4).setWeight(1),
							item(Items.BOOK, 1, 3).setWeight(2),
							item(Items.GOLD_INGOT, 1, 2).setWeight(1),
							tagEntry(DPTags.Items.LOOT_TOWER_UNCOMMON, 1, 2)
						)).setRolls(UniformGenerator.between(2, 3)),
					pool(ImmutableList.of(
							item(Items.INFESTED_STONE_BRICKS, 1, 3).setWeight(3),
							item(Items.BOOK, 1, 2).setWeight(2),
							item(Items.ENDER_PEARL, 1, 2).setWeight(1),
							item(Items.SADDLE).setWeight(2),
							item(Items.NAME_TAG).setWeight(1),
							item(Items.BOOK).setWeight(1).apply(enchant()),
							tagEntry(DPTags.Items.LOOT_TOWER_RARE)
						)).setRolls(UniformGenerator.between(0, 2)))));
			
			consumer.accept(DPLoot.Tower.CHEST_BARREL, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.SUGAR, 1, 3).setWeight(2),
							item(Items.BUCKET).setWeight(1),
							item(Items.POTION).setWeight(1).apply(SetPotionFunction.setPotion(Potions.WATER)),
							item(Items.WHEAT, 1, 4).setWeight(2),
							item(Items.BREAD, 1, 2).setWeight(2),
							item(Items.STICK, 2, 3).setWeight(2),
							item(Items.WOODEN_SWORD).setWeight(1),
							item(Items.IRON_NUGGET, 2, 5).setWeight(1),
							item(Items.COBBLESTONE, 2, 4).setWeight(3),
							item(Items.BOWL).setWeight(1),
							item(Items.RED_MUSHROOM, 1, 2).setWeight(1),
							item(Items.BROWN_MUSHROOM, 2, 3).setWeight(2)
						)).setRolls(UniformGenerator.between(3, 7)))));
			
			consumer.accept(DPLoot.Tower.CHEST_VEX, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.FEATHER, 1, 2).setWeight(1),
							item(Items.IRON_NUGGET, 4, 9).setWeight(1),
							item(Items.IRON_INGOT, 1, 5).setWeight(1),
							item(Items.DARK_OAK_SAPLING, 1, 3).setWeight(1),
							item(Items.LEATHER, 1, 3).setWeight(1),
							tagEntry(DPTags.Items.LOOT_TOWER_UNCOMMON, 1, 2)
						)).setRolls(UniformGenerator.between(3, 5)),
					pool(ImmutableList.of(
							item(Items.LEATHER_HORSE_ARMOR).setWeight(2),
							item(Items.GOLD_INGOT, 2, 4).setWeight(5),
							item(Items.PHANTOM_MEMBRANE, 1, 3).setWeight(4),
							item(Items.IRON_AXE).setWeight(1),
							item(Items.IRON_AXE).setWeight(1),
							item(Items.IRON_SHOVEL).setWeight(1),
							item(Items.IRON_HOE).setWeight(1),
							item(Items.IRON_PICKAXE).setWeight(1),
							item(Items.IRON_SWORD).setWeight(1),
							item(Items.SPYGLASS).setWeight(2),
							item(Items.SOUL_SAND, 2, 3).setWeight(4),
							tagEntry(DPTags.Items.LOOT_TOWER_RARE)
						)).setRolls(UniformGenerator.between(2, 4)))));

			consumer.accept(DPLoot.Tower.CHEST_VEX_MAP, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.MAP).setWeight(1).apply(map(DPTags.Structures.ON_REANIMATED_RUINS_MAPS)).apply(mapName(DPStructures.REANIMATED_RUINS))
						)),
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(DPLoot.Tower.CHEST_VEX)
						)))));
			//@formatter:on
		}

		private void reanimatedRuins(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
		{
			//@formatter:off
			consumer.accept(DPLoot.ReanimatedRuins.CHEST_COMMON, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.BONE, 1, 3).setWeight(1),
							item(Items.GUNPOWDER, 1, 2).setWeight(1),
							item(Items.ROTTEN_FLESH, 2, 4).setWeight(1),
							item(Items.STRING, 1, 3).setWeight(1),
							item(Items.FEATHER, 1, 2).setWeight(1),
							item(Items.STONE_BRICKS, 3, 5).setWeight(1),
							tagEntry(DPTags.Items.LOOT_REANIMATED_RUINS_COMMON, 1, 3)
						)).setRolls(UniformGenerator.between(3, 6)),
					pool(ImmutableList.of(
							item(Items.IRON_NUGGET, 1, 3).setWeight(3),
							item(Items.REDSTONE, 1, 3).setWeight(2),
							item(Items.HONEYCOMB, 1, 2).setWeight(1),
							item(Items.BOOK, 1, 3).setWeight(2),
							item(Items.GOLD_NUGGET, 2, 4).setWeight(1),
							tagEntry(DPTags.Items.LOOT_REANIMATED_RUINS_UNCOMMON, 1, 2)
						)).setRolls(UniformGenerator.between(2, 3)),
					pool(ImmutableList.of(
							item(Items.BREAD, 1, 3).setWeight(4),
							item(Items.ENDER_PEARL, 0, 1).setWeight(2),
							item(Items.SADDLE).setWeight(2),
							item(Items.NAME_TAG).setWeight(1),
							item(Items.BOOK).setWeight(1).apply(enchant()),
							tagEntry(DPTags.Items.LOOT_REANIMATED_RUINS_RARE)
						)).setRolls(UniformGenerator.between(0, 2)))));
			
			consumer.accept(DPLoot.ReanimatedRuins.CHEST_DESERT, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.SAND, 1, 2).setWeight(1),
							item(Items.BONE, 1, 2).setWeight(1),
							item(Items.ROTTEN_FLESH, 1, 5).setWeight(1),
							item(Items.DEAD_BUSH).setWeight(1),
							item(Items.GOLD_NUGGET, 2, 5).setWeight(1),
							item(Items.IRON_INGOT, 1, 3).setWeight(1),
							tagEntry(DPTags.Items.LOOT_REANIMATED_RUINS_COMMON, 1, 3)
						)).setRolls(UniformGenerator.between(3, 5)),
					pool(ImmutableList.of(
							item(Items.RED_SAND, 2, 3).setWeight(2),
							item(Items.GLASS_BOTTLE).setWeight(1),
							item(Items.GOLD_INGOT, 2, 4).setWeight(4),
							item(Items.REDSTONE, 1, 3).setWeight(4),
							item(Items.COAL, 1, 3).setWeight(4),
							item(Items.IRON_SHOVEL).setWeight(1),
							tagEntry(DPTags.Items.LOOT_REANIMATED_RUINS_UNCOMMON, 1, 2)
						)).setRolls(UniformGenerator.between(2, 4)),
					pool(ImmutableList.of(
							item(Items.GOLDEN_APPLE).setWeight(1),
							item(Items.BUCKET).setWeight(2),
							item(Items.BOOK).setWeight(5).apply(enchant()),
							item(Items.GOLD_NUGGET).setWeight(5),
							tagEntry(DPTags.Items.LOOT_REANIMATED_RUINS_RARE)
						)).setRolls(UniformGenerator.between(1, 3)))));
			
			consumer.accept(DPLoot.ReanimatedRuins.CHEST_DESERT_MAP, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.MAP).setWeight(1).apply(map(DPTags.Structures.ON_LEVIATHAN_MAPS)).apply(mapName(DPStructures.LEVIATHAN))
						)),
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(DPLoot.ReanimatedRuins.CHEST_DESERT)
						)))));
			
			consumer.accept(DPLoot.ReanimatedRuins.CHEST_FROZEN, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.ARROW, 1, 5).setWeight(3),
							item(Items.BONE, 2, 5).setWeight(3),
							item(Items.SNOWBALL, 1, 3).setWeight(2),
							item(Items.FLINT, 1, 2).setWeight(1),
							item(Items.FEATHER, 1, 2).setWeight(1),
							item(Items.STRING, 1, 2).setWeight(1),
							tagEntry(DPTags.Items.LOOT_REANIMATED_RUINS_COMMON, 1, 3)
						)).setRolls(UniformGenerator.between(3, 5)),
					pool(ImmutableList.of(
							item(Items.ICE, 2, 3).setWeight(2),
							item(Items.TIPPED_ARROW, 2, 4).setWeight(4).apply(SetPotionFunction.setPotion(Potions.SLOWNESS)),
							item(Items.IRON_INGOT, 2, 4).setWeight(4),
							item(Items.REDSTONE, 1, 3).setWeight(4),
							item(Items.COAL, 1, 3).setWeight(4),
							item(Items.BOW).setWeight(1),
							tagEntry(DPTags.Items.LOOT_REANIMATED_RUINS_UNCOMMON, 1, 2)
						)).setRolls(UniformGenerator.between(2, 4)),
					pool(ImmutableList.of(
							item(Items.BOW).setWeight(2).apply(enchant()),
							item(Items.CHAINMAIL_CHESTPLATE).setWeight(2),
							item(Items.BOOK).setWeight(5).apply(enchant()),
							item(Items.MUSIC_DISC_WAIT).setWeight(1),
							tagEntry(DPTags.Items.LOOT_REANIMATED_RUINS_RARE)
						)).setRolls(UniformGenerator.between(1, 3)))));
			
			consumer.accept(DPLoot.ReanimatedRuins.CHEST_FROZEN_MAP, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.MAP).setWeight(1).apply(map(DPTags.Structures.ON_SNOWY_TEMPLE_MAPS)).apply(mapName(DPStructures.SNOWY_TEMPLE))
						)),
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(DPLoot.ReanimatedRuins.CHEST_FROZEN)
						)))));
			
			consumer.accept(DPLoot.ReanimatedRuins.CHEST_MOSSY, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.ROTTEN_FLESH, 1, 5).setWeight(3),
							item(Items.GLOW_BERRIES, 2, 5).setWeight(3),
							item(Items.SMALL_DRIPLEAF, 1, 3).setWeight(2),
							item(Items.RAW_COPPER, 1, 2).setWeight(1),
							item(Items.CLAY_BALL, 1, 2).setWeight(1),
							item(Items.STRING, 1, 2).setWeight(1),
							tagEntry(DPTags.Items.LOOT_REANIMATED_RUINS_COMMON, 1, 3)
						)).setRolls(UniformGenerator.between(3, 5)),
					pool(ImmutableList.of(
							item(Items.MOSS_BLOCK, 2, 3).setWeight(2),
							item(Items.SLIME_BALL, 2, 4).setWeight(4),
							item(Items.GOLD_NUGGET, 1, 4).setWeight(3),
							item(Items.RAW_COPPER, 1, 2).setWeight(3),
							item(Items.COAL, 1, 3).setWeight(4),
							item(Items.MINECART).setWeight(1),
							tagEntry(DPTags.Items.LOOT_REANIMATED_RUINS_UNCOMMON, 1, 2)
						)).setRolls(UniformGenerator.between(2, 4)),
					pool(ImmutableList.of(
							item(Items.RAW_COPPER_BLOCK, 0, 2).setWeight(5),
							item(Items.WATER_BUCKET).setWeight(1),
							item(Items.BOOK).setWeight(2).apply(enchant()),
							item(Items.IRON_PICKAXE).setWeight(1).apply(enchant(Enchantments.BLOCK_EFFICIENCY, Enchantments.UNBREAKING, Enchantments.SILK_TOUCH, Enchantments.VANISHING_CURSE)).apply(setDamage(10, 60)),
							tagEntry(DPTags.Items.LOOT_REANIMATED_RUINS_RARE)
						)).setRolls(UniformGenerator.between(1, 3)))));
			
			consumer.accept(DPLoot.ReanimatedRuins.CHEST_MOSSY_MAP, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.MAP).setWeight(1).apply(map(DPTags.Structures.ON_WARPED_GARDEN_MAPS)).apply(mapName(DPStructures.WARPED_GARDEN))
						)),
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(DPLoot.ReanimatedRuins.CHEST_MOSSY)
						)))));
			
			//@formatter:on
		}

		private void leviathan(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
		{
			//@formatter:off
			consumer.accept(DPLoot.Leviathan.CHEST_COMMON, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.BONE, 1, 2).setWeight(10),
							item(Items.RABBIT_HIDE, 1, 2).setWeight(10),
							item(Items.ROTTEN_FLESH, 1, 5).setWeight(10),
							item(Items.GOLD_NUGGET).setWeight(10),
							item(Items.SAND, 1, 2).setWeight(10),
							item(Items.DEAD_BUSH).setWeight(1),
							tagEntry(DPTags.Items.LOOT_LEVIATHAN_COMMON, 1, 3)
						)).setRolls(UniformGenerator.between(4, 7)),
					pool(ImmutableList.of(
							item(Items.QUARTZ, 1, 5).setWeight(1),
							item(Items.LAPIS_LAZULI, 1, 5).setWeight(2),
							item(Items.GOLD_INGOT, 2, 4).setWeight(5),
							item(Items.EMERALD, 1, 3).setWeight(5),
							item(Items.GOLDEN_SHOVEL).setWeight(3).apply(enchant()),
							tagEntry(DPTags.Items.LOOT_LEVIATHAN_UNCOMMON, 1, 2)
						)).setRolls(UniformGenerator.between(3, 4)),
					pool(ImmutableList.of(
							item(Items.GOLDEN_APPLE).setWeight(1),
							item(Items.EXPERIENCE_BOTTLE, 1, 2).setWeight(3),
							item(Items.GOLDEN_HORSE_ARMOR).setWeight(1),
							item(Items.NAME_TAG).setWeight(1),
							item(Items.BOOK).setWeight(3).apply(enchant()),
							tagEntry(DPTags.Items.LOOT_LEVIATHAN_RARE)
						)).setRolls(UniformGenerator.between(2, 3)))));
			
			consumer.accept(DPLoot.Leviathan.CHEST_RARE, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(DPItems.LEVIATHAN_BLADE.get()).setWeight(1)
						)),
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(DPLoot.Leviathan.CHEST_COMMON)
						)))));
			//@formatter:on
		}

		private void snowyTemple(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
		{
			//@formatter:off
			consumer.accept(DPLoot.SnowyTemple.CHEST_COMMON, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.ICE, 1, 2).setWeight(1),
							item(Items.SNOWBALL, 1, 2).setWeight(1),
							item(Items.ARROW, 1, 4).setWeight(1),
							item(Items.BONE, 1, 4).setWeight(1),
							item(Items.FLINT, 1, 2).setWeight(1),
							item(Items.FEATHER, 1, 2).setWeight(1),
							tagEntry(DPTags.Items.LOOT_SNOWY_TEMPLE_COMMON, 1, 3)
						)).setRolls(UniformGenerator.between(4, 7)),
					pool(ImmutableList.of(
							item(Items.PACKED_ICE, 1, 5).setWeight(1),
							item(Items.TIPPED_ARROW, 2, 4).setWeight(2).apply(SetPotionFunction.setPotion(Potions.SLOWNESS)),
							item(Items.IRON_INGOT, 1, 3).setWeight(5),
							item(Items.LAPIS_LAZULI, 1, 3).setWeight(5),
							item(Items.BOW).setWeight(1).apply(enchant()),
							item(Items.POWDER_SNOW_BUCKET, 1, 2).setWeight(1),
							tagEntry(DPTags.Items.LOOT_SNOWY_TEMPLE_UNCOMMON, 1, 2)
						)).setRolls(UniformGenerator.between(3, 4)),
					pool(ImmutableList.of(
							item(Items.EXPERIENCE_BOTTLE, 1, 2).setWeight(3),
							item(Items.CHAINMAIL_CHESTPLATE).setWeight(1),
							item(Items.CHAINMAIL_LEGGINGS).setWeight(1),
							item(Items.BOOK).setWeight(3).apply(enchant()),
							item(Items.LEATHER_BOOTS).setWeight(1).apply(enchant(Enchantments.FROST_WALKER, Enchantments.UNBREAKING, Enchantments.VANISHING_CURSE)),
							tagEntry(DPTags.Items.LOOT_SNOWY_TEMPLE_RARE)
						)).setRolls(UniformGenerator.between(2, 3)))));

			consumer.accept(DPLoot.SnowyTemple.CHEST_RARE, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(DPItems.FROSTED_COWL.get()).setWeight(1)
						)),
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(DPLoot.SnowyTemple.CHEST_COMMON)
						)))));
			//@formatter:on
		}

		private void warpedGarden(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
		{
			//@formatter:off
			consumer.accept(DPLoot.WarpedGarden.CHEST_COMMON, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.ROTTEN_FLESH, 1, 3).setWeight(1),
							item(Items.WARPED_ROOTS, 1, 2).setWeight(1),
							item(Items.WARPED_FUNGUS, 1, 2).setWeight(1),
							item(Items.GOLD_NUGGET, 1, 3).setWeight(1),
							item(Items.QUARTZ, 1, 3).setWeight(1),
							tagEntry(DPTags.Items.LOOT_WARPED_GARDEN_COMMON, 1, 3)
						)).setRolls(UniformGenerator.between(4, 7)),
					pool(ImmutableList.of(
							item(Items.NETHERRACK, 1, 2).setWeight(1),
							item(Items.POTION, 1, 3).setWeight(1).apply(SetPotionFunction.setPotion(Potions.WATER)),
							item(Items.SCUTE, 1, 3).setWeight(1),
							item(Items.WATER_BUCKET).setWeight(1),
							item(Items.GOLD_INGOT, 1, 3).setWeight(3),
							tagEntry(DPTags.Items.LOOT_WARPED_GARDEN_UNCOMMON, 1, 2)
						)).setRolls(UniformGenerator.between(3, 4)),
					pool(ImmutableList.of(
							item(Items.NAUTILUS_SHELL).setWeight(3),
							item(Items.NETHER_GOLD_ORE, 5, 11).setWeight(10),
							item(Items.PUFFERFISH).setWeight(7),
							item(Items.BOOK).setWeight(5).apply(enchant()),
							item(Items.ENCHANTED_GOLDEN_APPLE).setWeight(1),
							tagEntry(DPTags.Items.LOOT_WARPED_GARDEN_RARE)
						)).setRolls(UniformGenerator.between(2, 3)))));
			
			consumer.accept(DPLoot.WarpedGarden.CHEST_RARE, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(DPItems.WARPED_AXE.get()).setWeight(1)
						)),
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(DPLoot.WarpedGarden.CHEST_COMMON)
						)))));
			//@formatter:on
		}

		private void soulPrison(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
		{
			//@formatter:off
			consumer.accept(DPLoot.SoulPrison.CHEST_COMMON, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.BONE, 1, 2).setWeight(1),
							item(Items.ARROW, 1, 2).setWeight(1),
							item(Items.FIRE_CHARGE, 1, 2).setWeight(1),
							item(Items.GOLD_INGOT, 1, 3).setWeight(2),
							item(Items.GUNPOWDER, 1, 2).setWeight(2),
							tagEntry(DPTags.Items.LOOT_SOUL_PRISON_COMMON, 1, 3)
						)).setRolls(UniformGenerator.between(4, 7)),
					pool(ImmutableList.of(
							item(Items.GOLDEN_PICKAXE).setWeight(1),
							item(Items.SOUL_LANTERN, 1, 2).setWeight(1).apply(SetPotionFunction.setPotion(Potions.WATER)),
							item(Items.SOUL_SOIL, 1, 3).setWeight(2),
							item(Items.GLOWSTONE, 1, 2).setWeight(2),
							item(Items.BONE_BLOCK, 1, 2).setWeight(3),
							item(Items.GHAST_TEAR, 1, 2).setWeight(3),
							tagEntry(DPTags.Items.LOOT_SOUL_PRISON_UNCOMMON, 1, 2)
						)).setRolls(UniformGenerator.between(3, 4)),
					pool(ImmutableList.of(
							item(Items.DIAMOND, 1, 2).setWeight(3),
							item(Items.MUSIC_DISC_11).setWeight(4),
							item(Items.BOOK).setWeight(4).apply(enchant()),
							item(Items.GOLDEN_CARROT, 2, 4).setWeight(5),
							LootTableReference.lootTableReference(DPLoot.SoulPrison.CHEST_GOLDEN_ARMOR)
						)).setRolls(UniformGenerator.between(2, 3)))));
			
			consumer.accept(DPLoot.SoulPrison.CHEST_GOLDEN_ARMOR, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.GOLDEN_HELMET).setWeight(2).apply(enchant(Enchantments.BLAST_PROTECTION, Enchantments.FIRE_PROTECTION, Enchantments.UNBREAKING)),
							item(Items.GOLDEN_CHESTPLATE).setWeight(2).apply(enchant(Enchantments.BLAST_PROTECTION, Enchantments.FIRE_PROTECTION, Enchantments.UNBREAKING)),
							item(Items.GOLDEN_HELMET).setWeight(1).apply(enchant()),
							item(Items.GOLDEN_CHESTPLATE).setWeight(1).apply(enchant()),
							tagEntry(DPTags.Items.LOOT_SOUL_PRISON_RARE)
						)).setRolls(UniformGenerator.between(1, 2)))));
			
			consumer.accept(DPLoot.SoulPrison.CHEST_RARE, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(DPItems.SOUL_CANNON.get()).setWeight(1)
						)),
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(DPLoot.SoulPrison.CHEST_COMMON)
						)))));
			//@formatter:on
		}

		private void endRuins(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
		{

		}
	}

	private static class DPEntityLoot extends EntityLootSubProvider implements LootPoolUtil
	{
		public DPEntityLoot()
		{
			super(FeatureFlags.REGISTRY.allFlags());
		}

		@Override
		public void generate()
		{
		}

		@Override
		public void generate(BiConsumer<ResourceLocation, Builder> consumer)
		{
			//@formatter:off
			consumer.accept(DPLoot.Tower.ENTITY_ZOMBIE, table(ImmutableList.of(
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(EntityType.ZOMBIE.getDefaultLootTable())
						)).when(LootItemRandomChanceCondition.randomChance(0.5F)))));
			
			consumer.accept(DPLoot.Tower.ENTITY_SKELETON, table(ImmutableList.of(
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(EntityType.SKELETON.getDefaultLootTable())
						)).when(LootItemRandomChanceCondition.randomChance(0.5F)))));
			
			consumer.accept(DPLoot.Tower.ENTITY_SPIDER, table(ImmutableList.of(
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(EntityType.SPIDER.getDefaultLootTable())
						)).when(LootItemRandomChanceCondition.randomChance(0.5F)))));
			
			consumer.accept(DPLoot.ReanimatedRuins.ENTITY_ZOMBIE, table(ImmutableList.of(
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(EntityType.ZOMBIE.getDefaultLootTable())
						)).when(LootItemRandomChanceCondition.randomChance(0.25F)))));
			
			consumer.accept(DPLoot.ReanimatedRuins.ENTITY_SKELETON, table(ImmutableList.of(
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(EntityType.SKELETON.getDefaultLootTable())
						)).when(LootItemRandomChanceCondition.randomChance(0.25F)))));
			
			consumer.accept(DPLoot.Leviathan.ENTITY_HUSK, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.SAND, 0, 2).setWeight(1).apply(LootingEnchantFunction.lootingMultiplier(UniformGenerator.between(0, 1)))
						)),
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(EntityType.HUSK.getDefaultLootTable())
						)))));
			
			consumer.accept(DPLoot.SnowyTemple.ENTITY_STRAY, table(ImmutableList.of(
					pool(ImmutableList.of(
							item(Items.ICE, 0, 2).setWeight(128).apply(LootingEnchantFunction.lootingMultiplier(UniformGenerator.between(0, 1))),
							item(Items.PACKED_ICE, 0, 2).setWeight(1).apply(LootingEnchantFunction.lootingMultiplier(UniformGenerator.between(0, 1)))
						)),
					pool(ImmutableList.of(
							LootTableReference.lootTableReference(EntityType.STRAY.getDefaultLootTable())
						)))));
			
			//@formatter:on
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Stream<EntityType<?>> getKnownEntityTypes()
		{
			return BuiltInRegistries.ENTITY_TYPE.stream().filter(e ->
			{
				var name = BuiltInRegistries.ENTITY_TYPE.getKey(e);
				return name != null && name.getNamespace().equals(DungeonsPlus.MODID);
			});
		}
	}

	private static class DPBlockLoot extends BlockLootSubProvider implements LootPoolUtil
	{
		protected DPBlockLoot()
		{
			super(Set.of(), FeatureFlags.REGISTRY.allFlags());
		}

		private void blockLoot(Block block)
		{
			if (block == DPBlocks.GRANITE_IRON_ORE.get())
				this.add(block, this.createOreDrop(block, Items.RAW_IRON));
			else if (block == DPBlocks.GRANITE_GOLD_ORE.get())
				this.add(block, this.createOreDrop(block, Items.RAW_GOLD));
			else
				this.dropSelf(block);
		}

		@Override
		protected void generate()
		{
			this.getKnownBlocks().forEach(this::blockLoot);
		}

		@Override
		protected Iterable<Block> getKnownBlocks()
		{
			return blocks()::iterator;
		}

		@SuppressWarnings("deprecation")
		private Stream<Block> blocks()
		{
			return BuiltInRegistries.BLOCK.stream().filter(b ->
			{
				var name = BuiltInRegistries.BLOCK.getKey(b);
				return name != null && name.getNamespace().equals(DungeonsPlus.MODID) && !b.getLootTable().equals(BuiltInLootTables.EMPTY);
			});
		}
	}

	public static final ResourceLocation LEVIATHAN_ARCHAEOLOGY_COMMON = DungeonsPlus.locate("leviathan_archaeology_common");
	public static final ResourceLocation LEVIATHAN_ARCHAEOLOGY_RARE = DungeonsPlus.locate("leviathan_archaeology_rare");

	private static class DPArchaeologyLoot implements LootTableSubProvider, LootPoolUtil
	{
		@Override
		public void generate(BiConsumer<ResourceLocation, Builder> output)
		{
			//@formatter:off
			
			// Copies of their respective vanilla archaeology loot tables with some modifications
			output.accept(LEVIATHAN_ARCHAEOLOGY_COMMON, LootTable.lootTable()
					.withPool(
							LootPool.lootPool()
							.setRolls(ConstantValue.exactly(1.0F))
							.add(LootItem.lootTableItem(Items.DANGER_POTTERY_SHERD))
							.add(LootItem.lootTableItem(Items.MINER_POTTERY_SHERD))
							.add(LootItem.lootTableItem(Items.PRIZE_POTTERY_SHERD))
							.add(LootItem.lootTableItem(Items.SKULL_POTTERY_SHERD))
							.add(LootItem.lootTableItem(Items.COPPER_INGOT))
							.add(LootItem.lootTableItem(Items.LAPIS_LAZULI))
							.add(LootItem.lootTableItem(Items.GUNPOWDER))
							.add(LootItem.lootTableItem(Items.EMERALD))
							.add(LootItem.lootTableItem(Items.BONE))));

			
			output.accept(LEVIATHAN_ARCHAEOLOGY_RARE, LootTable.lootTable()
					.withPool(
							LootPool.lootPool()
							.setRolls(ConstantValue.exactly(1.0F))
							.add(LootItem.lootTableItem(Items.DIAMOND))
							.add(LootItem.lootTableItem(Items.GOLD_INGOT))
							.add(LootItem.lootTableItem(Items.TNT))
							.add(LootItem.lootTableItem(Items.EMERALD))
							.add(LootItem.lootTableItem(Items.IRON_SWORD).apply(this.setDamage(70, 150)))));
			//@formatter:on
		}
	}

	private static interface LootPoolUtil
	{
		LootItemCondition.Builder SILK_TOUCH = MatchTool.toolMatches(ItemPredicate.Builder.item().hasEnchantment(new EnchantmentPredicate(Enchantments.SILK_TOUCH, MinMaxBounds.Ints.atLeast(1))));
		LootItemCondition.Builder SHEARS = MatchTool.toolMatches(ItemPredicate.Builder.item().of(Items.SHEARS));
		LootItemCondition.Builder SILK_TOUCH_OR_SHEARS = SHEARS.or(SILK_TOUCH);
		LootItemCondition.Builder NOT_SILK_TOUCH_OR_SHEARS = SILK_TOUCH_OR_SHEARS.invert();
		float[] DEFAULT_SAPLING_DROP_RATES = new float[] { 0.05F, 0.0625F, 0.083333336F, 0.1F };

		default String modID()
		{
			return DungeonsPlus.MODID;
		}

		/**
		 * Creates a table from the given loot pools.
		 * 
		 * @param pools
		 * @return
		 */
		default LootTable.Builder table(List<LootPool.Builder> pools)
		{
			LootTable.Builder table = LootTable.lootTable();
			pools.forEach(pool -> table.withPool(pool));
			return table;
		}

		/**
		 * Creates a table from the given loot pool.
		 * 
		 * @param pool
		 * @return
		 */
		default LootTable.Builder table(LootPool.Builder pool)
		{
			return LootTable.lootTable().withPool(pool);
		}

		default LootTable.Builder table(LootPool.Builder... pools)
		{
			LootTable.Builder table = LootTable.lootTable();
			for (var pool : pools)
				table.withPool(pool);
			return table;
		}

		default LootPool.Builder pool(ItemLike item, int min, int max, float chance)
		{
			return LootPool.lootPool().add(item(item, min, max).when(this.randomChance(chance)));
		}

		default LootPool.Builder pool(ItemLike item, int min, int max)
		{
			return LootPool.lootPool().add(item(item, min, max));
		}

		default LootPool.Builder pool(ItemLike item)
		{
			return LootPool.lootPool().add(item(item));
		}

		/**
		 * Creates a loot pool with multiple entries. One of these entries will be
		 * picked at random each time the pool rolls.
		 */
		default LootPool.Builder pool(LootPoolEntryContainer.Builder<?>... lootEntries)
		{
			LootPool.Builder pool = LootPool.lootPool();
			for (var entry : lootEntries)
				pool.add(entry);
			return pool;
		}

		default LootPool.Builder pool(Collection<LootPoolEntryContainer.Builder<?>> lootEntries)
		{
			LootPool.Builder pool = LootPool.lootPool();
			for (var entry : lootEntries)
				pool.add(entry);
			return pool;
		}

		/**
		 * Creates a loot pool that will give a random item from the list.
		 */
		default LootPool.Builder randomItemPool(List<ItemLike> items)
		{
			return pool(items.stream().map((i) -> item(i)).toArray(LootPoolEntryContainer.Builder[]::new));
		}

		/**
		 * Multiplies the item count by `rand(minLooting to maxLooting) * lootingLevel`
		 */
		default LootPool.Builder lootingPool(ItemLike item, int min, int max, int minLooting, int maxLooting)
		{
			return pool(item, min, max).apply(this.looting(minLooting, maxLooting));
		}

		default LootPool.Builder lootingPool(ItemLike item, int min, int max, int minLooting, int maxLooting, float chance)
		{
			return lootingPool(item, min, max, minLooting, maxLooting).when(this.randomChance(chance));
		}

		/**
		 * Creates a loot entry for the given item. Gives an amount between the min and
		 * max.
		 * 
		 * @param item
		 * @param min
		 * @param max
		 * @return
		 */
		default LootItem.Builder<?> item(ItemLike item, int min, int max)
		{
			return item(item).apply(setCount(min, max));
		}

		/**
		 * Creates a loot entry for the given item. Will only give one item.
		 * 
		 * @param item
		 * @return
		 */
		default LootItem.Builder<?> item(ItemLike item)
		{
			return LootItem.lootTableItem(item);
		}

		default LootItem.Builder<?> tagEntry(TagKey<Item> tag, int min, int max)
		{
			return TagEntry.expandTag(tag).apply(SetItemCountFunction.setCount(UniformGenerator.between(min, max)));
		}

		default LootItem.Builder<?> tagEntry(TagKey<Item> tag, int count)
		{
			return TagEntry.expandTag(tag).apply(SetItemCountFunction.setCount(ConstantValue.exactly(count)));
		}

		default LootItem.Builder<?> tagEntry(TagKey<Item> tag)
		{
			return tagEntry(tag, 1);
		}

		/**
		 * Sets the damage of the item (percentage)
		 * 
		 * @param min
		 *            0 - 100
		 * @param max
		 *            0 - 100
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> setDamage(int min, int max)
		{
			return SetItemDamageFunction.setDamage(UniformGenerator.between(min / 100F, max / 100F));
		}

		/**
		 * Cooks the item if the predicate passes
		 * 
		 * @param predicate
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> smeltItem(EntityPredicate.Builder predicate)
		{
			return SmeltItemFunction.smelted().when(LootItemEntityPropertyCondition.hasProperties(LootContext.EntityTarget.THIS, predicate));
		}

		/**
		 * Enchants the item randomly between the levels provided
		 * 
		 * @param minLevel
		 * @param maxLevel
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> enchant(int minLevel, int maxLevel)
		{
			return EnchantWithLevelsFunction.enchantWithLevels(UniformGenerator.between(minLevel, maxLevel));
		}

		/**
		 * Enchants the item randomly with the enchantments passed
		 * 
		 * @param enchantments
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> enchant(Enchantment... enchantments)
		{
			EnchantRandomlyFunction.Builder func = new EnchantRandomlyFunction.Builder();
			for (Enchantment enchantment : enchantments)
				func.withEnchantment(enchantment);
			return func;
		}

		/**
		 * Sets the nbt of the item
		 * 
		 * @param nbt
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> setNbt(Consumer<CompoundTag> nbt)
		{
			return SetNbtFunction.setTag(Util.make(new CompoundTag(), nbt));
		}
		
		default LootItemConditionalFunction.Builder<?> map(TagKey<Structure> structure)
		{
			return ExplorationMapFunction.makeExplorationMap().setDestination(structure).setMapDecoration(MapDecoration.Type.RED_X).setZoom((byte) 1).setSkipKnownStructures(false);
		}

		default LootItemConditionalFunction.Builder<?> mapName(StructureRegistrar<?> structure)
		{
			return SetNameFunction.setName(Component.translatable(DPLangProvider.mapName(structure)));
		}

		default LootItemConditionalFunction.Builder<?> setCount(float min, float max)
		{
			if (min == max)
				return setCount(min);
			return SetItemCountFunction.setCount(UniformGenerator.between(min, max));
		}

		default LootItemConditionalFunction.Builder<?> setCount(float count)
		{
			return SetItemCountFunction.setCount(ConstantValue.exactly(count));
		}

		default LootItemCondition.Builder playerKill()
		{
			return LootItemKilledByPlayerCondition.killedByPlayer();
		}

		default LootItemCondition.Builder randomChance(float chance)
		{
			return LootItemRandomChanceCondition.randomChance(chance);
		}

		default LootingEnchantFunction.Builder looting(float minLooting, float maxLooting)
		{
			return LootingEnchantFunction.lootingMultiplier(UniformGenerator.between(minLooting, maxLooting));
		}
	}
}
