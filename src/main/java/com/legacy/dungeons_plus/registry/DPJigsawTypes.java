package com.legacy.dungeons_plus.registry;

import com.legacy.dungeons_plus.DungeonsPlus;
import com.legacy.dungeons_plus.structures.end_ruins.EndRuinsStructure;
import com.legacy.dungeons_plus.structures.leviathan.LeviathanStructure;
import com.legacy.dungeons_plus.structures.reanimated_ruins.ReanimatedRuinsStructure;
import com.legacy.dungeons_plus.structures.warped_garden.WarpedGardenStructure;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.StructureGelRegistries;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapabilityType;

@RegistrarHolder
public class DPJigsawTypes
{
	public static final RegistrarHandler<JigsawCapabilityType<?>> HANDLER = RegistrarHandler.getOrCreate(StructureGelRegistries.Keys.JIGSAW_TYPE, DungeonsPlus.MODID);

	public static final Registrar.Static<JigsawCapabilityType<ReanimatedRuinsStructure.Capability>> REANIMATED_RUINS = HANDLER.createStatic("reanimated_ruins", () -> () -> ReanimatedRuinsStructure.Capability.CODEC);
	public static final Registrar.Static<JigsawCapabilityType<LeviathanStructure.Capability>> LEVIATHAN = HANDLER.createStatic("leviathan", () -> () -> LeviathanStructure.Capability.CODEC);
	public static final Registrar.Static<JigsawCapabilityType<WarpedGardenStructure.Capability>> WARPED_GARDEN = HANDLER.createStatic("warped_garden", () -> () -> WarpedGardenStructure.Capability.CODEC);
	public static final Registrar.Static<JigsawCapabilityType<EndRuinsStructure.Capability>> END_RUINS = HANDLER.createStatic("end_ruins", () -> () -> EndRuinsStructure.Capability.CODEC);
}
